import { Geoparnamirim2Page } from './app.po';

describe('geoparnamirim2 App', () => {
  let page: Geoparnamirim2Page;

  beforeEach(() => {
    page = new Geoparnamirim2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
