import {NgModule} from '@angular/core';
import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {PortalComponent} from './portal.component';

const portalRoutes: Routes = [
	{path: '', component: PortalComponent}
];



@NgModule({
	imports : [RouterModule.forChild(portalRoutes)],
	exports : [RouterModule]
})

export class PortalRoutingModule{
	
}	