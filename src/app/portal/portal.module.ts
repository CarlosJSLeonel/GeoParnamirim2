import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PortalRoutingModule} from './portal.routing.module';
import {PortalComponent} from './portal.component';
import { Ng2MapModule} from 'ng2-map';

@NgModule({
  imports: [
    CommonModule,
    PortalRoutingModule,
    Ng2MapModule.forRoot({apiUrl: 'http://maps.googleapis.com/maps/api/js?key=AIzaSyCA21Yr7erpWzBiBPbWrv893BKTOyg5tSk&libraries=places,visualization,drawing,geometry'})
  ],
  declarations: [PortalComponent]
})
export class PortalModule { }
