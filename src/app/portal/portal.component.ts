import { Component,ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-portal',
  templateUrl: './portal.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./portal.component.css']
})

export class PortalComponent {
   onMapReady(map) {
    console.log('map', map);
    console.log('markers', map.markers);  // to get all markers as an array 
  }
  onIdle(event) {
    console.log('map', event.target);
  }
  onMarkerInit(marker) {
    console.log('marker', marker);
  }
  clicked(t) {
  	console.log(t);
	    var e = t.target;
	    e.ng2MapComponent.openInfoWindow("iw", e, {
	        lat: e.getPosition().lat(),
	        lng: e.getPosition().lng()
	    })
	}
}
