import {NgModule} from '@angular/core';
import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {PortalComponent} from './portal/portal.component';
import {GerenciadorComponent} from './gerenciador/gerenciador.component';


const APP_ROUTES: Routes = [
	{
	 path: 'portal', 
	 component: PortalComponent
	},
	{
	path: 'gerenciador', 
	loadChildren: 'app/gerenciador/gerenciador.module#GerenciadorModule'
	}
];



@NgModule({
	imports : [RouterModule.forRoot(APP_ROUTES)],
	exports : [RouterModule]
})

export class AppRoutingModule{
	
}	