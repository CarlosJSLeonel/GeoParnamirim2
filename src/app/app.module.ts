import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { PortalModule} from './portal/portal.module';
import { GerenciadorModule} from './gerenciador/gerenciador.module';
import { AppComponent } from './app.component';
import {AppRoutingModule} from './app.routing.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    PortalModule,
    GerenciadorModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
