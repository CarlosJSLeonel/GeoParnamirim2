import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastroMarcadorComponent } from './cadastro-marcador.component';

describe('CadastroMarcadorComponent', () => {
  let component: CadastroMarcadorComponent;
  let fixture: ComponentFixture<CadastroMarcadorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadastroMarcadorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastroMarcadorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
