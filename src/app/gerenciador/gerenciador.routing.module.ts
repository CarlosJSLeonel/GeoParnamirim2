import {NgModule} from '@angular/core';
import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {CadastroCategoriaComponent} from './cadastro-categoria/cadastro-categoria.component';
import {CadastroMarcadorComponent} from './cadastro-marcador/cadastro-marcador.component';
import {MenuPrincipalComponent} from './menu-principal/menu-principal.component';
import {VinculoUsuarioComponent} from './vinculo-usuario/vinculo-usuario.component';

import {GerenciadorComponent} from './gerenciador.component';

const gerenciadorRoutes: Routes = [
	{path: '', component: GerenciadorComponent,
	 children: [
		{path: 'categoria', component: CadastroCategoriaComponent},
		{path: 'marcador', component: CadastroMarcadorComponent},
		{path: 'menu', component: MenuPrincipalComponent},
		{path: 'vinculo', component: VinculoUsuarioComponent}
	 ]}
];

@NgModule({
	imports : [RouterModule.forChild(gerenciadorRoutes)],
	exports : [RouterModule]
})

export class GerenciadorRoutingModule{
	
}	