import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GerenciadorRoutingModule} from './gerenciador.routing.module';

import { GerenciadorComponent } from './gerenciador.component';
import { CadastroCategoriaComponent } from './cadastro-categoria/cadastro-categoria.component';
import { CadastroMarcadorComponent } from './cadastro-marcador/cadastro-marcador.component';
import { MenuPrincipalComponent } from './menu-principal/menu-principal.component';
import { VinculoUsuarioComponent } from './vinculo-usuario/vinculo-usuario.component';

@NgModule({
  imports: [
    CommonModule,
    GerenciadorRoutingModule
  ],
  declarations: [GerenciadorComponent,
          CadastroCategoriaComponent, 
  				CadastroMarcadorComponent, 
  				MenuPrincipalComponent, 
  				VinculoUsuarioComponent
  				]
})
export class GerenciadorModule { }
