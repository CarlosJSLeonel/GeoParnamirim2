import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VinculoUsuarioComponent } from './vinculo-usuario.component';

describe('VinculoUsuarioComponent', () => {
  let component: VinculoUsuarioComponent;
  let fixture: ComponentFixture<VinculoUsuarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VinculoUsuarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VinculoUsuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
