"use strict";

function scrollToTop(element){
	if($(element).length) {
		$('html, body').stop();
        $('html, body').animate({
            scrollTop: $(element).offset().top
      	}, 500);
    }
}

var showMobile = false;

function exibirMenuMobile(){
    if (!showMobile){
    	showMobile=true;
        $("#menu-mobile").show();
        $('html, body').animate({
            scrollTop: $("body").offset().top
      	}, 500);
    }else{
        $("#menu-mobile").hide();
        showMobile=false;
    }
    
    scrollToTop();
}

function exibirMenu(show){
    if (show){
    	showMobile = true;
        $("#menu-mobile").show();
    }else{
    	showMobile = false;
        $("#menu-mobile").hide();
    }
}

$(function() {
	$(window).scroll(function(e){ 
		showMobile = false;
		
		if ($(this).scrollTop() >= 60){ 
		  $(".fixed-header").addClass("header-fixed");
		}
		
		if ($(this).scrollTop() < 60){
		  $(".fixed-header").removeClass("header-fixed");
		}

	    $(".fill").height($(".fill-clone").height());
 	});
});