$(document).ready(function(){
	$(".bloco-categoria").click(function(e){
        var menuOrientacao = $(this).closest("#menu").hasClass("menu-horizontal") ? true : false;

		var elementMenu = $(this).find(".menu-categoria");
		if($(this).hasClass("on"))
		{
			if(menuOrientacao)
				elementMenu.slideUp("medium");
			else
				elementMenu.slideUp("medium");
					
			$(this).removeClass("on");
			$("#menu").removeClass("on");	
		}
		else
		{
			$(".bloco-categoria").each(function(){
				if($(this).hasClass("on"))
				{
					if(menuOrientacao)
						$(this).find(".menu-categoria").slideUp("medium");
					else
						$(this).find(".menu-categoria").slideUp("medium");
					
					$(this).removeClass("on");
					$("#menu").removeClass("on");
				}
			});

			if(menuOrientacao)
				elementMenu.slideDown("medium");
			else
				elementMenu.slideDown("medium");

			$(this).addClass("on");
			$("#menu").addClass("on");
		}
	}).find('.menu-categoria').on('click', function (e) {
	  	e.stopPropagation();
	});

	$("body").click(function(){
		$(".bloco-categoria.on").click();	
	}).find('.bloco-categoria').on('click', function (e) {
	  	e.stopPropagation();
	});

   	$(".draggable").draggable({
   		delay: 200,		
		cursor: "move", 
		cursorAt: { top: 15, left: 50 },
   		start: function( event, ui ) {
   			$(this).addClass("elementOnDrag");
   			$(".droppable").addClass("on");
		},
		stop: function( event, ui ) {
			$(this).removeAttr("style");
			$(this).removeClass("elementOnDrag");
   			$(".droppable").removeClass("on");
		}
   	});

	$(".droppable").droppable({
    	hoverClass: "elementOnDragHover",
    	drop: function( event, ui ) {

			if ($(this).hasClass("top"))
			{
				$(".draggable")
        		.removeClass("menu-vertical right bottom left")
          		.addClass("menu-horizontal top");	
			}
			else if ($(this).hasClass("right"))
			{
				$(".draggable")
        		.removeClass( "menu-horizontal top bottom left" )
          		.addClass("menu-vertical right");	
			}
			else if ($(this).hasClass("bottom"))
			{
				$(".draggable")
        		.removeClass( "menu-vertical top right left" )
          		.addClass("menu-horizontal bottom");	
			}
			else
			{
				$(".draggable")
        		.removeClass( "menu-horizontal top right bottom" )
          		.addClass("menu-vertical left");	
			}
      	}
    });
});