package br.gov.rn.parnamirim.geoparnamirim.service;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;



@ApplicationPath("/rest")

public class RESTEasyStarter extends Application {
	private Set<Object> singletons = new HashSet<Object>();

	private Set<Class<?>> classes = new HashSet<Class<?>>();

	public RESTEasyStarter() throws Exception {
		this.singletons.add(new MarcadorService());
		this.singletons.add(new CategoriaService());
		this.singletons.add(new TipoService());

	}

	@Override
	public Set<Class<?>> getClasses() {
		return this.classes;
	}

	@Override
	public Set<Object> getSingletons() {
		return this.singletons;
	}
}
