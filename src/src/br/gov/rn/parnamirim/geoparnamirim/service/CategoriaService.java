package br.gov.rn.parnamirim.geoparnamirim.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import br.gov.rn.parnamirim.geoparnamirim.DAO.CategoriaDAO;

@Path("/categoria")
public class CategoriaService {
	
	@GET
	@Path("/listar")
	@Consumes("application/json")
	@Produces("application/json")
	public Response listaCategorias() {

		CategoriaDAO categoriaDAO = new CategoriaDAO();
		return Response.status(200).entity(categoriaDAO.listarCategorias()).build();
	}
}
