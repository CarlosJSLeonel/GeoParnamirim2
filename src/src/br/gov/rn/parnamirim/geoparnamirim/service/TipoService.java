package br.gov.rn.parnamirim.geoparnamirim.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import br.gov.rn.parnamirim.geoparnamirim.DAO.TipoDAO;

@Path("/tipo")
public class TipoService {
	
	@GET
	@Path("/listar")
	@Consumes("application/json")
	@Produces("application/json")
	public Response listaTipo() {

		TipoDAO tipoDAO = new TipoDAO();
		return Response.status(200).entity(tipoDAO.listarTipos()).build();
	}
}
