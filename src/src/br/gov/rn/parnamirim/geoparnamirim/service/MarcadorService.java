package br.gov.rn.parnamirim.geoparnamirim.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import br.gov.rn.parnamirim.geoparnamirim.DAO.MarcadorDAO;



@Path("/marcador")
public class MarcadorService {
	
	@GET
	@Path("/listar")
	@Consumes("application/json")
	@Produces("application/json")
	public Response listaMarcadores() {

		MarcadorDAO marcadorDAO = new MarcadorDAO();
		return Response.status(200).entity(marcadorDAO.listarMarcadores()).build();
	}
	
	@GET
	@Path("/listaru")
	@Consumes("application/json")
	@Produces("application/json")
	public Response listaMarcador() {

		MarcadorDAO marcadorDAO = new MarcadorDAO();
		return Response.status(200).entity(marcadorDAO.listarMarcador()).build();
	}
}
