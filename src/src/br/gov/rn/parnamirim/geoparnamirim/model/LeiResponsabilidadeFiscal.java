package br.gov.rn.parnamirim.geoparnamirim.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "geoparnamirim.lei_responsabilidade_fiscal")
public class LeiResponsabilidadeFiscal {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;
	
	@Column(name = "titulo")
	private String titulo;
	
	@Column(name = "ativo")
	private boolean ativo;
	
	@Column(name = "descricao")
	private boolean descricao;
	
	@Column(name = "ano_vigente")
	private int anoVigente;
	
	@Column(name = "objetivo")
	private String objetivo;
	
	@Column(name = "justificativa")
	private String justificativa;
	
	@ManyToOne
	@JoinColumn(name = "orgao_responsavel_lei_responsabilidade_fiscal_id")
	private OrgaoResponsavelLeiResponsabilidadeFiscal orgaoResponsavelLeiResponsabilidadeFiscal;
	
	@ManyToOne
	@JoinColumn(name = "programa_lei_responsabilidade_fiscal_id")
	private ProgramaLeiResponsabilidadeFiscal programaLeiResponsabilidadeFiscal;
	
	@ManyToOne
	@JoinColumn(name = "tipo_lei_responsabilidade_fiscal_id")
	private TipoLeiResponsabilidadeFiscal tipoLeiResponsabilidadeFiscal;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public boolean isDescricao() {
		return descricao;
	}

	public void setDescricao(boolean descricao) {
		this.descricao = descricao;
	}

	public int getAnoVigente() {
		return anoVigente;
	}

	public void setAnoVigente(int anoVigente) {
		this.anoVigente = anoVigente;
	}

	public String getObjetivo() {
		return objetivo;
	}

	public void setObjetivo(String objetivo) {
		this.objetivo = objetivo;
	}

	public String getJustificativa() {
		return justificativa;
	}

	public void setJustificativa(String justificativa) {
		this.justificativa = justificativa;
	}

	public OrgaoResponsavelLeiResponsabilidadeFiscal getOrgaoResponsavelLeiResponsabilidadeFiscal() {
		return orgaoResponsavelLeiResponsabilidadeFiscal;
	}

	public void setOrgaoResponsavelLeiResponsabilidadeFiscal(
			OrgaoResponsavelLeiResponsabilidadeFiscal orgaoResponsavelLeiResponsabilidadeFiscal) {
		this.orgaoResponsavelLeiResponsabilidadeFiscal = orgaoResponsavelLeiResponsabilidadeFiscal;
	}

	public ProgramaLeiResponsabilidadeFiscal getProgramaLeiResponsabilidadeFiscal() {
		return programaLeiResponsabilidadeFiscal;
	}

	public void setProgramaLeiResponsabilidadeFiscal(ProgramaLeiResponsabilidadeFiscal programaLeiResponsabilidadeFiscal) {
		this.programaLeiResponsabilidadeFiscal = programaLeiResponsabilidadeFiscal;
	}

	public TipoLeiResponsabilidadeFiscal getTipoLeiResponsabilidadeFiscal() {
		return tipoLeiResponsabilidadeFiscal;
	}

	public void setTipoLeiResponsabilidadeFiscal(TipoLeiResponsabilidadeFiscal tipoLeiResponsabilidadeFiscal) {
		this.tipoLeiResponsabilidadeFiscal = tipoLeiResponsabilidadeFiscal;
	}

}
