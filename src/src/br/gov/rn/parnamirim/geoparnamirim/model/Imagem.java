package br.gov.rn.parnamirim.geoparnamirim.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

@Entity
@Table(name = "geoparnamirim.imagem")
public final class Imagem {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;

	@Column(name = "caminho")
	private String caminho;

//	@Column(name = "status")
//	private boolean status;
	@ManyToOne(fetch = FetchType.EAGER)
	@JsonIgnore
	@JoinColumn(name = "marcador_id")
	private Marcador marcador;

//	@Column(name = "log_user")
//	private long logUser;
//
//	@Column(name = "log_date")
//	private Date logDate = new Date();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCaminho() {
		return caminho;
	}

	public void setCaminho(String caminho) {
		this.caminho = caminho;
	}

//	public boolean isStatus() {
//		return status;
//	}
//
//	public void setStatus(boolean status) {
//		this.status = status;
//	}

	public Marcador getMarcador() {
		return marcador;
	}

	public void setMarcador(Marcador marcador) {
		this.marcador = marcador;
	}

//	public long getLogUser() {
//		return logUser;
//	}
//
//	public void setLogUser(long logUser) {
//		this.logUser = logUser;
//	}
//
//	public Date getLogDate() {
//		return logDate;
//	}
//
//	public void setLogDate(Date logDate) {
//		this.logDate = logDate;
//	}

}
