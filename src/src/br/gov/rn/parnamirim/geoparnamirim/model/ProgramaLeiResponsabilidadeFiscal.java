package br.gov.rn.parnamirim.geoparnamirim.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "geoparnamirim.programa_lei_responsabilidade_fiscal")
public class ProgramaLeiResponsabilidadeFiscal {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;

	@Column(name = "codigo")
	private long codigo;

	@Column(name = "titulo_programa")
	private String tituloPrograma;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getCodigo() {
		return codigo;
	}

	public void setCodigo(long codigo) {
		this.codigo = codigo;
	}

	public String getTituloPrograma() {
		return tituloPrograma;
	}

	public void setTituloPrograma(String tituloPrograma) {
		this.tituloPrograma = tituloPrograma;
	}
	
	

}
