package br.gov.rn.parnamirim.geoparnamirim.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "geoparnamirim.acao_lei_responsabilidade_fiscal")
public final class AcaoLeiResponsabilidadeFiscal {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;

	@Column(name = "codigo_acao")
	private long codigoAcao;

	@Column(name = "descricao_acao")
	private String descricaoAcao;

	@Column(name = "gerente")
	private String gerente;

	@Column(name = "produto")
	private String produto;

	@Column(name = "unidade_medida")
	private String unidadeMedida;

	@Column(name = "meta_fisica")
	private int metaFisica;

	@Column(name = "custo_estimado")
	private double custoEstimado;
	
	@ManyToOne
	@JoinColumn(name = "ppa_id")
	private LeiResponsabilidadeFiscal ppa;

	@Column(name = "titulo")
	private String titulo;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getCodigoAcao() {
		return codigoAcao;
	}

	public void setCodigoAcao(long codigoAcao) {
		this.codigoAcao = codigoAcao;
	}

	public String getDescricaoAcao() {
		return descricaoAcao;
	}

	public void setDescricaoAcao(String descricaoAcao) {
		this.descricaoAcao = descricaoAcao;
	}

	public String getGerente() {
		return gerente;
	}

	public void setGerente(String gerente) {
		this.gerente = gerente;
	}

	public String getProduto() {
		return produto;
	}

	public void setProduto(String produto) {
		this.produto = produto;
	}

	public String getUnidadeMedida() {
		return unidadeMedida;
	}

	public void setUnidadeMedida(String unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}

	public int getMetaFisica() {
		return metaFisica;
	}

	public void setMetaFisica(int metaFisica) {
		this.metaFisica = metaFisica;
	}

	public double getCustoEstimado() {
		return custoEstimado;
	}

	public void setCustoEstimado(double custoEstimado) {
		this.custoEstimado = custoEstimado;
	}

	public LeiResponsabilidadeFiscal getLeiResponsabilidadeFiscal() {
		return ppa;
	}

	public void setLeiResponsabilidadeFiscal(LeiResponsabilidadeFiscal ppa) {
		this.ppa = ppa;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}


}
