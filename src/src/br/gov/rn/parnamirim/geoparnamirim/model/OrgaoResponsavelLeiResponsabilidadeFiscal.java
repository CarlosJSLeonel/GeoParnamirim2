package br.gov.rn.parnamirim.geoparnamirim.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "geoparnamirim.orgao_responsavel_lei_responsabilidade_fiscal")
public class OrgaoResponsavelLeiResponsabilidadeFiscal {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;

	@Column(name = "titulo_orgao")
	private String tituloOrgao;

	@Column(name = "codigo")
	private String codigo;

	@Column(name = "sigla")
	private String sigla;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTituloOrgao() {
		return tituloOrgao;
	}

	public void setTituloOrgao(String tituloOrgao) {
		this.tituloOrgao = tituloOrgao;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	
	
}
