package br.gov.rn.parnamirim.geoparnamirim.testes;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import br.gov.rn.parnamirim.geoparnamirim.DAO.MarcadorDAO;
import br.gov.rn.parnamirim.geoparnamirim.model.Atributo;
import br.gov.rn.parnamirim.geoparnamirim.model.AtributoMarcador;
import br.gov.rn.parnamirim.geoparnamirim.model.Categoria;
import br.gov.rn.parnamirim.geoparnamirim.model.Imagem;
import br.gov.rn.parnamirim.geoparnamirim.model.Marcador;
import br.gov.rn.parnamirim.geoparnamirim.model.Tipo;
import junit.framework.Assert;

public class MarcadorTeste {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void listarMarcadoresTeste() {
		Atributo atributo = new Atributo();
		Atributo atributo2 = new Atributo();
		MarcadorDAO marcadoresDAO = new MarcadorDAO();
		Imagem imagem = new Imagem();
		AtributoMarcador atributoMarcador = new AtributoMarcador();
		List<Imagem> imagens = new ArrayList<Imagem>();
		List<AtributoMarcador> atributoMarcadores = new ArrayList<AtributoMarcador>();
		Marcador marcador = new Marcador();
		Categoria categoria = new Categoria();
		Tipo tipo = new Tipo();
		
		atributo.setId(1);
		atributo.setDescricao("test");
//		atributo2.setDescricao(descricao);
		atributoMarcador.setId(8);
		atributoMarcador.setValor("teste");
		
		atributoMarcadores.add(atributoMarcador);
		
		categoria.setNome("teste");
		
		tipo.setId(1);
		tipo.setNome("teste");
		tipo.setCategoria(categoria);
		
		imagem.setId(1);
		imagem.setCaminho("teste");
		
		imagens.add(imagem);

		marcador.setBairro("teste");
		marcador.setDescricao("teste");
		marcador.setDetalhes("teste");
		marcador.setLatitude("teste");
		marcador.setLongitude("teste");
		marcador.setRua("teste");
		marcador.setTitulo("teste");
		marcador.setAtributoMarcadores(atributoMarcadores);
		marcador.setImagens(imagens);
		marcador.setTipo(tipo);
		
		Assert.assertEquals(marcador, marcadoresDAO.listarMarcadores());
	}

}
