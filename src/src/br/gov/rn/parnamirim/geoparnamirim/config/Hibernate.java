package br.gov.rn.parnamirim.geoparnamirim.config;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import br.gov.rn.parnamirim.geoparnamirim.model.AcaoLeiResponsabilidadeFiscal;
import br.gov.rn.parnamirim.geoparnamirim.model.Atributo;
import br.gov.rn.parnamirim.geoparnamirim.model.AtributoMarcador;
import br.gov.rn.parnamirim.geoparnamirim.model.Categoria;
import br.gov.rn.parnamirim.geoparnamirim.model.Imagem;
import br.gov.rn.parnamirim.geoparnamirim.model.LeiResponsabilidadeFiscal;
import br.gov.rn.parnamirim.geoparnamirim.model.Marcador;
import br.gov.rn.parnamirim.geoparnamirim.model.OrgaoResponsavelLeiResponsabilidadeFiscal;
import br.gov.rn.parnamirim.geoparnamirim.model.ProgramaLeiResponsabilidadeFiscal;
import br.gov.rn.parnamirim.geoparnamirim.model.Tipo;
import br.gov.rn.parnamirim.geoparnamirim.model.TipoLeiResponsabilidadeFiscal;

/**
 *
 * @author chandu
 */
public class Hibernate {

	private static Hibernate me;
	private Configuration cfg;
	private SessionFactory sessionFactory;

	private Hibernate() throws HibernateException {

		// build the config
		cfg = new Configuration();

		/**
		 * Connection Information..
		 */
		cfg.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
		cfg.setProperty("hibernate.connection.driver_class", "org.postgresql.Driver");

		cfg.setProperty("hibernate.connection.url", "jdbc:postgresql://localhost:5432/db_new_site");
		cfg.setProperty("hibernate.connection.username", "postgres");
		cfg.setProperty("hibernate.connection.password", "123");

		//
		// cfg.setProperty("hibernate.connection.url",
		// "jdbc:postgresql://172.16.0.43:5667/db_sgi");
		// cfg.setProperty("hibernate.connection.username", "admin_db_sgi");
		// cfg.setProperty("hibernate.connection.password", "P0O9I87U");

		//
		cfg.setProperty("show_sql", "true");
		cfg.addAnnotatedClass(AcaoLeiResponsabilidadeFiscal.class);
		cfg.addAnnotatedClass(Atributo.class);
		cfg.addAnnotatedClass(AtributoMarcador.class);
		cfg.addAnnotatedClass(Categoria.class);
		cfg.addAnnotatedClass(Categoria.class);
		cfg.addAnnotatedClass(Imagem.class);
		cfg.addAnnotatedClass(LeiResponsabilidadeFiscal.class);
		cfg.addAnnotatedClass(Marcador.class);
		cfg.addAnnotatedClass(OrgaoResponsavelLeiResponsabilidadeFiscal.class);
		cfg.addAnnotatedClass(ProgramaLeiResponsabilidadeFiscal.class);
		cfg.addAnnotatedClass(Tipo.class);
		cfg.addAnnotatedClass(TipoLeiResponsabilidadeFiscal.class);
		
		
		sessionFactory = cfg.buildSessionFactory();
	}

	public static synchronized Hibernate getInstance() throws HibernateException {
		if (me == null) {
			me = new Hibernate();
		}

		return me;
	}

	public Session getSession() throws HibernateException {
		Session session = sessionFactory.openSession();
		if (!session.isConnected()) {
			this.reconnect();
		}
		return session;
	}

	private void reconnect() throws HibernateException {
		this.sessionFactory = cfg.buildSessionFactory();
	}
}