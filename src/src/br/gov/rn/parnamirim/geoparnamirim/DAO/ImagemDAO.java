package br.gov.rn.parnamirim.geoparnamirim.DAO;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import br.gov.rn.parnamirim.geoparnamirim.model.Imagem;

public class ImagemDAO extends DAO{
		Transaction tr = null;
		Session session = null;
		
		public List<Imagem> listarImagens(){
			List<Imagem> imagens = new ArrayList<Imagem>();
			try {
				session = getHibernateSession();
				tr = session.beginTransaction();
				Query query = session.createQuery("FROM Imagem");
				imagens = query.list();
				tr.commit();
			} catch (Exception e) {
				e.printStackTrace();
				tr.rollback();
			}finally {
				session.close();
			}
			return imagens;
		}
}
