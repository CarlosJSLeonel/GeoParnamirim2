package br.gov.rn.parnamirim.geoparnamirim.DAO;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import br.gov.rn.parnamirim.geoparnamirim.model.Categoria;

public class CategoriaDAO extends DAO {

	Transaction tr = null;
	Session session = null;
	long returnId = 1L;

	public long cadastrarCategoria(Categoria categoria) {

		Categoria _categoria = new Categoria();
		_categoria = verificarCategoriaExistente(categoria);

		try {
			session = getHibernateSession();
			if (_categoria == null) {
				tr = session.beginTransaction();
				returnId = (long) session.save(categoria);
				tr.commit();
			} else {
				tr = session.beginTransaction();
				_categoria.setNome(categoria.getNome());
				returnId = _categoria.getId();
				session.update(_categoria);
				tr.commit();
			}
		} catch (Exception e) {
			e.printStackTrace();
			tr.rollback();
		} finally {
			session.close();
		}
		return returnId;
	}

	public List<Categoria> listarCategorias(){
		List<Categoria> categorias = new ArrayList<Categoria>();
	try {
		session = getHibernateSession();
		tr = session.beginTransaction();
		categorias = session.createQuery("FROM Categoria").list();
		tr.commit();
	} catch (Exception e) {
		e.printStackTrace();
		tr.rollback();
	}finally {
		session.close();
	}
	return categorias;
}

	public Categoria verificarCategoriaExistente(Categoria categoria) {

		try {
			session = getHibernateSession();
			Query query = session.createQuery("FROM Categoria c WHERE c.id = :id").setParameter("id",
					categoria.getId());
			categoria = (Categoria) query.uniqueResult();
		} catch (Exception e) {

			e.printStackTrace();
		} finally {
			session.close();
		}
		return categoria;
	}

}
