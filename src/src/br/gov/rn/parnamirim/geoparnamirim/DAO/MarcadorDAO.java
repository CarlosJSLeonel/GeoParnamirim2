package br.gov.rn.parnamirim.geoparnamirim.DAO;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import br.gov.rn.parnamirim.geoparnamirim.model.Marcador;

public class MarcadorDAO extends DAO{
	Transaction tr = null;
	Session session = null;
	
	public long cadastrarMarcador(Marcador marcador){
		AtributoMarcadorDAO atributoMarcadoresDAO = new AtributoMarcadorDAO();
		ImagemDAO imagemDAO = new ImagemDAO();
		TipoDAO tipoDAO = new TipoDAO();
		long returnId = 1L;
		
		
//		marcador.getAtributosMarcadores().setId(atributoMarcadoresDAO
//				.cadastrarAtributoMarcador(marcador.getAtributosMarcadores()));
		
		try {
			session = getHibernateSession();
			tr = session.beginTransaction();
			
			session.update(marcador);
			
			tr.commit();
		} catch (Exception e) {
			tr.rollback();
			e.printStackTrace();

		}finally{
			session.close();
		}
		return returnId;
	}
	
	public List<Marcador> listarMarcadores(){
		List<Marcador> marcadores = new ArrayList<Marcador>();
		
		try {
			session = getHibernateSession();
			tr = session.beginTransaction();
			
			marcadores = session.createQuery("FROM Marcador").list();

			tr.commit();
			
		} catch (Exception e) {
			e.printStackTrace();
			tr.rollback();
		}finally {
			session.close();
		}
		
		return marcadores;
	}
	
	public Marcador listarMarcador(){
		Marcador marcador = new Marcador();
		
		try {
			session = getHibernateSession();
			tr = session.beginTransaction();
			Query query = session.createQuery("FROM Marcador");
			marcador = (Marcador)query.uniqueResult();
			tr.commit();
		} catch (Exception e) {
			e.printStackTrace();
			tr.rollback();
		}finally {
			session.close();
		}
		
		return marcador;
	}
}
