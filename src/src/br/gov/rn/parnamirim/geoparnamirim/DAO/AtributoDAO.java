package br.gov.rn.parnamirim.geoparnamirim.DAO;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import br.gov.rn.parnamirim.geoparnamirim.model.Atributo;

public class AtributoDAO extends DAO{
	Transaction tr = null;
	Session session = null;
	
	public long cadastrarAtributo(Atributo atributo){
		
		long returnId = -1L;
		
		Atributo _atributo = new Atributo();
		_atributo = verificarAtributoExistente(atributo);
		
		try {
			session = getHibernateSession();
			
			if (_atributo == null){
				tr = session.beginTransaction();
				returnId = (long)session.save(atributo);
				tr.commit();
			}else{
				tr = session.beginTransaction();
				_atributo.setDescricao(atributo.getDescricao());
				returnId = _atributo.getId();
				session.update(_atributo);
				tr.commit();
			}
		} catch (Exception e) {
			e.printStackTrace();
			tr.rollback();
		}finally {
			session.close();
		}
		return returnId;
	}
	
	public Atributo verificarAtributoExistente(Atributo atributo){
		
		try {
			session = getHibernateSession();
			Query query = session.createQuery("FROM Atributo a WHERE a.id = :id")
					.setParameter("id", atributo.getId());
			atributo = (Atributo)query.uniqueResult();
		} catch (Exception e) {
			
			e.printStackTrace();
		}finally{
			session.close();
		}
		return atributo;
	}
	
}
