package br.gov.rn.parnamirim.geoparnamirim.DAO;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;


import br.gov.rn.parnamirim.geoparnamirim.model.AtributoMarcador;

public class AtributoMarcadorDAO extends DAO{
		Transaction tr = null;
		Session session = null;
	
		public long cadastrarAtributoMarcador(List<AtributoMarcador> atributoMarcadores){
			MarcadorDAO marcadorDAO = new MarcadorDAO();
			AtributoDAO atributoDAO = new AtributoDAO();
		
//			List<AtributoMarcador> encaminhamentos = new ArrayList<AtributoMarcador>();
			long returnId = -1L;
			
//			atributoMarcadores.getAtributo().setId(atributoDAO.cadastrarAtributo(atributoMarcador.getAtributo()));
//			atributoMarcadores.getCategoria().setId(categoriaDAO.cadastrarCategoria(atributoMarcador.getCategoria()));
//			atributoMarcadores.getMarcador().setId(marcadorDAO.cadastrarMarcador(atributoMarcador.getMarcador()));
			
			try {
				session = getHibernateSession();
				tr = session.beginTransaction();
				returnId = (long)session.save(atributoMarcadores);
				tr.commit();

			} catch (Exception e) {
				e.printStackTrace();
				tr.rollback();
			}finally {
				session.close();
			}
			return returnId;
		}
		
//		public AtributoMarcador verificarAtributoMarcadorExistente(AtributoMarcador atributoMarcador){
//			
//			try {
//				session = getHibernateSession();
//				Query query = session.createQuery("FROM AtributoMarcador a WHERE a.id = :id")
//						.setParameter("id", atributoMarcador.getId());
//				atributoMarcador = (AtributoMarcador)query.uniqueResult();
//			} catch (Exception e) {
//				
//				e.printStackTrace();
//			}finally{
//				session.close();
//			}
//			return atributoMarcador;
//		}
		public List<AtributoMarcador> listarAtributoMarcadores(){
			List<AtributoMarcador> atributoMarcadores = new ArrayList<AtributoMarcador>();
			
			try {
				session = getHibernateSession();
				tr = session.beginTransaction();
				Query query = session.createQuery("From AtributoMarcador");
				atributoMarcadores = query.list();
				tr.commit();
			} catch (Exception e) {
				e.printStackTrace();
				tr.rollback();
			}finally {
				session.close();
			}
					
			return atributoMarcadores;
		}
}
