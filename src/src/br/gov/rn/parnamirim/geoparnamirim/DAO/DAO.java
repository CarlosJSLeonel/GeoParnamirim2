package br.gov.rn.parnamirim.geoparnamirim.DAO;

import org.hibernate.Session;

import br.gov.rn.parnamirim.geoparnamirim.config.Hibernate;

public class DAO {
	protected Session getHibernateSession() {
		return Hibernate.getInstance().getSession();
	}
}