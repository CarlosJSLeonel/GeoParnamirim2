package br.gov.rn.parnamirim.geoparnamirim.DAO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import br.gov.rn.parnamirim.geoparnamirim.model.Tipo;

public class TipoDAO extends DAO{
	
	Transaction tr = null;
	Session session = null;
	
	public List<Tipo> listarTipos(){
		 List<Tipo> tipos = new ArrayList<Tipo>();
		 
		 try {
			session = getHibernateSession();
			tr = session.beginTransaction();
			tipos = session.createQuery("FROM Tipo").list();
			tr.commit();
		} catch (Exception e) {
			e.printStackTrace();
			tr.rollback();
		}finally{
			session.close();
		}
		 return tipos;
	}
}
